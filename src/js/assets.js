const insertValue = (selector, value) => {
    const elements = document.querySelectorAll(selector);
    elements.forEach(el => {
        el.innerHTML = value;
    });
};

const buildRow = asset => {
    const tr = document.createElement('tr');
    tr.innerHTML = `
        <td>${asset.id}</td>
        <td>${asset.name}</td>
        <td>${asset.manufacturer}</td>
        <td>${asset.type}</td>
        <td>
            <a href="/user/${asset.assignee.id}">${asset.assignee.name}</a>
        </td>
        <td>
            <a href="/location/${asset.location.id}">${asset.location.location || asset.location.name}</a>
        </td>
        <td>${asset.issues.total} (${asset.issues.open} open / ${asset.issues.closed} closed)</td>
        <td>
            <a href="./asset-view.html">View</a>
            <a href="#">Create Ticket</a>
        </td>
    `;
    return tr;
};

const displayResults = results => {
    const tableBody = document.querySelector('#assets tbody');
    if( tableBody ){
        tableBody.innerHTML = '';
        results.forEach(asset => {
            const tr = asset ? buildRow(asset) : null;
            if( tr ){
                tableBody.appendChild( tr );
            }
        });
    };
};

const getResults = (phrase = '') => {
    return fetch('http://localhost:8080/data/assets.json')
        .then(d => d.json())
        .then(data => data.assets || {})
        .then(assets => {
            return assets.filter(asset => {
                if( phrase ){
                    const name = asset && asset.name ? asset.name : '';
                    return name.includes(phrase);
                }
                return true;
            });
        });
};

const searchFormSubmit = (e) => {
    e.preventDefault();
    const phrase = e.target.phrase.value;
    getResults(phrase).then(data => displayResults(data));
};

const startedResults = () => {
    getResults().then(data => displayResults(data));
};

const registerSearchForm = () => {
    const searchForm = document.querySelector('#searchForm');
    if( searchForm ){
        searchForm.addEventListener('submit', searchFormSubmit);
    };
};

document.addEventListener('DOMContentLoaded', startedResults);
document.addEventListener('DOMContentLoaded', registerSearchForm);
