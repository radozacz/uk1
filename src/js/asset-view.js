const rowIssue = row => {
    const tr = document.createElement('tr');
    tr.innerHTML = `
        <td>${row.id}</td>
        <td>${row.title}</td>
        <td>
            <a href="./asset-view.html">
                ${row.asset.name}
            </a>
        </td>
        <td>
            <a href="./user/${row.reportedBy.id}">
                ${row.reportedBy.name}
            </a>
        </td>
        <td>
            <a href="./user/${row.assignedTo.id}">
                ${row.assignedTo.id}
            </a>
        </td>
        <td>
            <a href="./ticket/ ${row.id}">
                View
            </a>
        </td>
    `;
    return tr;
};

const displayIssues = (wrapperSelector, issues) => {
    const wrapper = document.querySelector(wrapperSelector);
    if( wrapper ){
        const counter = wrapper.querySelector('[data-issues="count"]');
        const tableBody = wrapper.querySelector('[data-issues="rows"]');
        counter.innerHTML = `(${issues.length})`;
        if( tableBody ){
            tableBody.innerHTML = '';
            issues.forEach(issue => {
                const tr = issue ? rowIssue(issue) : null;
                if( tr ){
                    tableBody.append(tr);
                }
            });
        }
    };
};

const displayDetails = (data) => {
    const wrapper = document.querySelector('#details');
    if( wrapper ){
        wrapper.innerHTML = `
            <h2>${data.name}</h2>
            <div class="mb-2">
                <a href="#" type="button" class="btn btn-success">Edit</a>
                <a href="#" type="button" class="btn btn-info">View Audit</a>
                <a href="#" type="button" class="btn btn-danger">Create Ticket</a>
            </div>
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td>Manufacturer:</td>
                        <td>${data.manufacturer}</td>
                    </tr>
                    <tr>
                        <td>Type:</td>
                        <td>${data.type}</td>
                    </tr>
                    <tr>
                        <td>Allocated To:</td>
                        <td><a href="./user/${data.assignee.id}">${data.assignee.name}</a></td>
                    </tr>
                    <tr>
                        <td>Purchase Date:</td>
                        <td>${data.purchaseDate}</td>
                    </tr>
                    <tr>
                        <td>Purchase Price:</td>
                        <td>${data.purchasePrice}</td>
                    </tr>
                    <tr>
                        <td>Location:</td>
                        <td><a href="./location/${data.location.id}">${data.location.location || data.location.name}</a></td>
                    </tr>
                    <tr>
                        <td>Asset Tag:</td>
                        <td>${data.assetTag}</td>
                    </tr>
                    <tr>
                        <td>Last PAT Test:</td>
                        <td>${data.lastPatTest}</td>
                    </tr>
                    <tr>
                        <td>Description:</td>
                        <td>${data.description}</td>
                    </tr>
                </tbody>
            </table>
        `
    };
};

const displayOpenIssue = (data) => {
    displayIssues('#openIssues', data);
};

const displayClosedIssue = (data) => {
    displayIssues('#closedIssues', data);
};

const fetchData = () => {
    return fetch('http://localhost:8080/data/asset-view.json')
        .then(d => d.json())
        .then(data => data || {});
};

const displayData = () => {
    fetchData()
        .then(data => {
            displayDetails(data);
            displayOpenIssue(data.issues.open);
            displayClosedIssue(data.issues.closed);
        });
};

document.addEventListener('DOMContentLoaded', displayData);
