const insertValue = (selector, value) => {
    const elements = document.querySelectorAll(selector);
    elements.forEach(el => {
        el.innerHTML = value;
    });
};

const insertDashboardData = () => {
    fetch('http://localhost:8080/data/dashboard.json')
        .then(d => d.json())
        .then(dashboardData => {
            Object.entries(dashboardData).forEach(([key,value]) => {
                insertValue(`[data-dashboard="${key}"]`, value);
            });
            insertValue(`[data-sla="count"]`, `(${dashboardData.pastSla.count})`);
            const tableBody = document.querySelector('#pastSLA tbody');
            if( tableBody ){
                tableBody.innerHTML = '';
                dashboardData.pastSla.issues.forEach(issue => {
                    const tr = document.createElement('tr');
                    tr.innerHTML = `
                        <td>${issue.id}</td>
                        <td>${issue.title}</td>
                        <td>
                            <a href="./asset-view.html">
                                ${issue.asset.name}
                            </a>
                        </td>
                        <td>
                            <a href="./user/${issue.reportedBy.id}">
                                ${issue.reportedBy.name}
                            </a>
                        </td>
                        <td>
                            <a href="./user/${issue.assignedTo.id}">
                                ${issue.assignedTo.name}
                            </a>
                        </td>
                        <td>
                            <a href="./ticket/${issue.id}">
                                View
                            </a>
                        </td>
                    `;
                    tableBody.appendChild(tr);
                });
            }
        });
};

const onPageLoaded = () => {
    insertDashboardData();
};

document.addEventListener('DOMContentLoaded', onPageLoaded);
