var path = require('path');

module.exports = {
    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
    entry: './src/js/app.js',
    output: {
        path: path.resolve(__dirname, 'public/theme'),
        filename: 'app.bundle.js'
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './public'
    }
};
